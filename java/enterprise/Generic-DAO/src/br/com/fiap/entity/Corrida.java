package br.com.fiap.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="T_CORRIDA")
@SequenceGenerator(name="corrida", sequenceName="T_SQ_CORRIDA", allocationSize=1)
public class Corrida {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="corrida")
	@Column(name="cd_corrida")
	private int codigo;
	
	@Column(name="ds_origem", length=150)
	private String origem;
	
	@Column(name="ds_destino", length=150)
	private String destino;
	
	@Temporal(TemporalType.DATE)
	@Column(name="dt_corrida")
	private Calendar data;
	
	@Column(name="vl_corrida", nullable=false)
	private float valor;
	
	public Corrida(){
		super();
	}
	
	//create
	public Corrida(String origem, String destino, Calendar data, float valor){
		super();
		this.origem = origem;
		this.destino = destino;
		this.data = data;
		this.valor = valor;
	}
	
	//update
	public Corrida(int codigo, String origem, String destino, Calendar data, float valor){
		super();
		this.codigo = codigo;
		this.origem = origem;
		this.destino = destino;
		this.data = data;
		this.valor = valor;
	}
	
}
