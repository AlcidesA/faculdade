package br.com.fiap.teste;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.fiap.dao.VeiculoDAO;
import br.com.fiap.entity.Veiculo;
import br.com.fiap.exception.CommitException;
import br.com.fiap.exception.NotFound;
import br.com.fiap.impl.VeiculoDAOImpl;

public class TesteVeiculo {

	public static void main(String[] args) {
		
		EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("CLIENTE_ORACLE");
		EntityManager em = fabrica.createEntityManager();
		
		VeiculoDAO dao = new VeiculoDAOImpl(em);
		Veiculo veiculo = new Veiculo(1,"GJI-0509","Azul", 2018);
		
		try {
			dao.cadastrar(veiculo);
			dao.commit();
		} catch (CommitException e) {
			e.printStackTrace();
		}
		
		
		veiculo.setAno(2019);
		try {
			dao.atualizar(veiculo);
			dao.commit();
		} catch (CommitException e) {
			e.printStackTrace();
		}
		
		
		try {
			Veiculo busca = dao.consultar(1);
			System.out.println(busca.getPlaca());
		} catch (NotFound e) {
			e.printStackTrace();
		}
		
		try {
			dao.remover(1);
			dao.commit();
		} catch (NotFound e) {
			e.printStackTrace();
		} catch (CommitException e) {
			e.printStackTrace();
		}
		
		
		
	}
	
}
