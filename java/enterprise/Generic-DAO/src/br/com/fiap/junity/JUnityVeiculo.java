package br.com.fiap.junity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertThrows;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.com.fiap.dao.VeiculoDAO;
import br.com.fiap.entity.Veiculo;
import br.com.fiap.exception.CommitException;
import br.com.fiap.exception.NotFound;
import br.com.fiap.impl.VeiculoDAOImpl;

class JUnityVeiculo {
	
	VeiculoDAO dao;
	Veiculo veiculo;
	
	@BeforeEach
	void instanciar() {
		EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("teste");
		EntityManager em = fabrica.createEntityManager();
		
		dao = new VeiculoDAOImpl(em);
		veiculo = new Veiculo(1,"ABC-123","Rosa", 2000);
		
		try {
			dao.cadastrar(veiculo);
			dao.commit();
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falha ao cadastrar o veiculo");
		}
		
	}
	
	@Test
	void cadastro() {	
		
		try {
			Veiculo busca = dao.consultar(1);
			assertNotNull(busca);
		} catch (NotFound e) {
			e.printStackTrace();
			fail("Veiculo não encontrado");
		}
		

	}
	
	@Test
	void buscar() {
		
		try {
			Veiculo busca = dao.consultar(1);
			assertNotNull(busca);
		} catch (NotFound e) {
			e.printStackTrace();
			fail("Veiculo não encontrado");
		}
		
		
	}
	
	@Test
	void atualizar() {
		
		veiculo.setPlaca("BBB-321");
		
		try {
			dao.atualizar(veiculo);
			dao.commit();
			
			assertEquals("BBB-321", veiculo.getPlaca());
		} catch (CommitException e) {
			e.printStackTrace();
			fail("Falha ao atualizar o veiculo");
		}
		
		
	}
	
	@Test
	void deletar(){
		
		try {
			dao.remover(1);
			dao.commit();
		} catch (NotFound e) {
			e.printStackTrace();
			fail("Falha ao remover");
		} catch (CommitException e) {
			e.printStackTrace();
			fail("Falha ao commitar");
		}
		
		assertThrows(NotFound.class, () -> dao.consultar(1) );
		
		
	}

}
