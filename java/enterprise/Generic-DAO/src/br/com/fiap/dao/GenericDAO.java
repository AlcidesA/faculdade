package br.com.fiap.dao;

import br.com.fiap.exception.CommitException;
import br.com.fiap.exception.NotFound;

//T = TABELA K = CHAVE
public interface GenericDAO<T, K> {
	
	void cadastrar(T entidade);
	
	T consultar(K chave) throws NotFound;

	void atualizar(T entidade);
	
	void remover(K chave) throws NotFound;
	 
	void commit() throws CommitException;
	
}
