package br.com.fiap.impl;

import javax.persistence.EntityManager;

import br.com.fiap.dao.MusicaDAO;
import br.com.fiap.entity.Musica;
import br.com.fiap.exception.ChaveInexistenteException;
import br.com.fiap.exception.CommitException;

public class MusicaDAOImpl extends GenericDAOImpl<Musica, Integer> implements MusicaDAO {

	public MusicaDAOImpl(EntityManager em) {
		super(em);
	}
	
}
