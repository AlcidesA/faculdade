package br.com.fiap.exception;

public class ChaveInexistenteException extends Exception{

	public ChaveInexistenteException() {
		super();
	}

	public ChaveInexistenteException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public ChaveInexistenteException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ChaveInexistenteException(String arg0) {
		super(arg0);
	}

	public ChaveInexistenteException(Throwable arg0) {
		super(arg0);
	}

}
