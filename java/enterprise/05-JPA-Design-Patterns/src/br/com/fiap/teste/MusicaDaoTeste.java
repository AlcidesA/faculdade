package br.com.fiap.teste;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.jupiter.api.Test;

import br.com.fiap.dao.MusicaDAO;
import br.com.fiap.entity.Genero;
import br.com.fiap.entity.Musica;
import br.com.fiap.exception.ChaveInexistenteException;
import br.com.fiap.impl.MusicaDAOImpl;

class MusicaDaoTeste {

	@Test
	void buscar() {
		//Cadastrando primeiro para depois buscar pois o teste � unitario!!!
		//Arrange
		EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("teste");
		EntityManager em = fabrica.createEntityManager();
		
		MusicaDAO dao = new MusicaDAOImpl(em);
		Musica musica = new Musica("Teste", 4, Genero.PAGODE);
		
		//Act - chamar o metodo que sera testado
		
		try {
			dao.cadastrar(musica);
			dao.commit();	
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falha no teste");
		}
		
		try {
			Musica busca = dao.consultar(musica.getCodigo());
			//Assert 
			assertNotNull(busca);
		} catch (ChaveInexistenteException e) {
			e.printStackTrace();
			fail("Falha na busca");
		}
		
		
	}
	
	@Test
	void cadastrar() {
		//Arrange - instanciar os objetos
		EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("teste");
		EntityManager em = fabrica.createEntityManager();
		
		MusicaDAO dao = new MusicaDAOImpl(em);
		Musica musica = new Musica("Teste", 4, Genero.PAGODE);
		
		//Act - chamar o metodo que sera testado
		
		try {
			dao.cadastrar(musica);
			dao.commit();	
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falha no teste");
		}
		
		//Assert - valida��o do resultado
		assertNotEquals(0, musica.getCodigo());
		
		
	}

}
