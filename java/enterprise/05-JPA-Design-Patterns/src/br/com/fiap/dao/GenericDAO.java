package br.com.fiap.dao;

import br.com.fiap.exception.ChaveInexistenteException;
import br.com.fiap.exception.CommitException;

//T = TABELA K = CHAVE
public interface GenericDAO<T,K> {

	void cadastrar(T entidade);
	
	T consultar(K chave) throws ChaveInexistenteException;
	
	void atualizar(T entidade);
	
	void remover(K chave) throws ChaveInexistenteException;
	
	void commit() throws CommitException;
	
	
}
