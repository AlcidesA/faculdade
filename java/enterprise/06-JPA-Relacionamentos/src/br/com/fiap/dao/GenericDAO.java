package br.com.fiap.dao;

import br.com.fiap.exception.ChaveInvalidaException;
import br.com.fiap.exception.CommitException;

public interface GenericDAO<T,K> {

	void cadastrar(T entidade);
	
	void atualizar(T entidade);
	
	void remover(K codigo) throws ChaveInvalidaException;
	
	T buscar(K codigo) throws ChaveInvalidaException;
	
	void commit() throws CommitException;
	
}