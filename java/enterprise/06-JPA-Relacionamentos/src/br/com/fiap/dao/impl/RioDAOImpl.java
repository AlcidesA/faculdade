package br.com.fiap.dao.impl;

import javax.persistence.EntityManager;
import br.com.fiap.dao.RioDAO;
import br.com.fiap.entity.Rio;

public class RioDAOImpl extends GenericDAOImpl<Rio, Integer> implements RioDAO {

	public RioDAOImpl(EntityManager em) {
		super(em);
	} 

}
