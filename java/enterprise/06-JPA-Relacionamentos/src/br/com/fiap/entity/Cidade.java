package br.com.fiap.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "T_CIDADE")
@SequenceGenerator(name = "cidade", sequenceName = "SQ_T_CIDADE", allocationSize = 1)
public class Cidade {

	@Id
	@Column(name = "cd_cidade")
	@GeneratedValue(generator = "cidade", strategy = GenerationType.SEQUENCE)
	private int codigo;
	
	@ManyToMany(cascade=CascadeType.PERSIST)
	@JoinTable(name="T_CIDADE_RIO",
		joinColumns = @JoinColumn(name="cd_cidade"),
		inverseJoinColumns = @JoinColumn(name="cd_rio"))
	private List<Rio> rios;

	@ManyToOne
	@JoinColumn(name = "cd_estado")
	private Estado estado;

	@Column(name = "nm_cidade", nullable = false, length = 100)
	private String nome;

	@Column(name = "vl_pib")
	private double pib;
	
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getPib() {
		return pib;
	}

	public void setPib(double pib) {
		this.pib = pib;
	}

	public List<Rio> getRios() {
		return rios;
	}

	public void setRios(List<Rio> rios) {
		this.rios = rios;
	}

}
