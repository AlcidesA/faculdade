package br.com.fiap.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "T_RIO")
@SequenceGenerator(name = "rio", sequenceName = "SQ_T_RIO", allocationSize = 1)
public class Rio {

	@Id
	@Column(name = "cd_rio")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rio")
	private int codigo;
	
	@ManyToMany(mappedBy="rios")
	private List<Cidade> cidades;

	@Column(name = "nm_rio", nullable = false, length = 50)
	private String nome;

	@Column(name = "nr_extensao")
	private float extensao;

	public Rio(String nome, float extensao) {
		super();
		this.nome = nome;
		this.extensao = extensao;
	}

	public Rio() {
		super();
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public float getExtensao() {
		return extensao;
	}

	public void setExtensao(float extensao) {
		this.extensao = extensao;
	}

	public List<Cidade> getCidades() {
		return cidades;
	}

	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}
	
}
