package br.com.fiap.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "T_CAPITAL")
@SequenceGenerator(name = "capital", sequenceName = "SQ_T_CAPITAL", allocationSize = 1)
public class Capital {

	@Id
	@Column(name="cd_capital")
	@GeneratedValue(generator="capital", strategy=GenerationType.SEQUENCE)
	private int codigo;

	//cascade -> realiza a a��o configurada em cascata
	//fetch -> determina o momento que � carregado a rela��o
	@OneToOne(cascade=CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name="cd_estado", nullable=false)
	private Estado estado;

	@Column(name="nm_capital", nullable=false, length=100)
	private String nome;

	@Column(name="nr_habitantes")
	private int numeroHabitantes;
	
	public Capital(Estado estado, String nome, int numeroHabitantes) {
		super();
		this.estado = estado;
		this.nome = nome;
		this.numeroHabitantes = numeroHabitantes;
	}

	public Capital() {
		super();
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getNumeroHabitantes() {
		return numeroHabitantes;
	}

	public void setNumeroHabitantes(int numeroHabitantes) {
		this.numeroHabitantes = numeroHabitantes;
	}

}
