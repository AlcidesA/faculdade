package br.com.fiap.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "T_ESTADO")
@SequenceGenerator(name = "estado", sequenceName = "SQ_T_ESTADO", allocationSize = 1)
public class Estado {

	@Id
	@Column(name = "cd_estado")
	@GeneratedValue(generator = "estado", strategy = GenerationType.SEQUENCE)
	private int codigo;

	// mappedBy-> nome do atributo que mapeia a FK na classe capital
	@OneToOne(mappedBy = "estado")
	private Capital capital;

	@OneToMany(mappedBy = "estado", cascade = CascadeType.PERSIST)
	private List<Cidade> cidades = new ArrayList<Cidade>();

	@Column(name = "nm_estado", nullable = false, length = 100)
	private String nome;

	@Column(name = "ds_sigla", length = 2, nullable = false)
	private String sigla;

	public void addCidade(Cidade cidade) {
		// adicionar a cidade na lista
		cidades.add(cidade);
		// adicionar o estado na cidade
		cidade.setEstado(this);
	}

	public Estado(String nome, String sigla) {
		super();
		this.nome = nome;
		this.sigla = sigla;
	}

	public Estado() {
		super();
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Capital getCapital() {
		return capital;
	}

	public void setCapital(Capital capital) {
		this.capital = capital;
	}

	public List<Cidade> getCidades() {
		return cidades;
	}

	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

}