package br.com.fiap.exception;

public class ChaveInvalidaException extends Exception {

	public ChaveInvalidaException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ChaveInvalidaException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ChaveInvalidaException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ChaveInvalidaException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ChaveInvalidaException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
