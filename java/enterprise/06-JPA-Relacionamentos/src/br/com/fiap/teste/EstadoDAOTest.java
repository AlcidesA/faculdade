package br.com.fiap.teste;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import br.com.fiap.dao.CapitalDAO;
import br.com.fiap.dao.EstadoDAO;
import br.com.fiap.dao.impl.CapitalDAOImpl;
import br.com.fiap.dao.impl.EstadoDAOImpl;
import br.com.fiap.entity.Capital;
import br.com.fiap.entity.Cidade;
import br.com.fiap.entity.Estado;
import br.com.fiap.entity.Rio;
import br.com.fiap.exception.ChaveInvalidaException;
import br.com.fiap.exception.CommitException;
import br.com.fiap.singleton.EntityManagerFactorySingleton;

class EstadoDAOTest {

	private static EstadoDAO estadoDao;
	private static CapitalDAO capitalDao;
	
	@BeforeAll //m�todo que executa antes de todos os testes
	public static void inicializar() {
		EntityManager em = EntityManagerFactorySingleton
			.getInstance().createEntityManager();
		
		estadoDao = new EstadoDAOImpl(em);
		capitalDao = new CapitalDAOImpl(em);
	}
	
	@Test
	void test() {
		
		//Cadastrar o estado e a capital
		Estado estado = new Estado("Acre", "AC");
		Capital capital = new Capital(estado, "Rio Branco", 1000);
		
		Cidade c1 = new Cidade();
		c1.setNome("Cidade 1");
		
		Cidade c2 = new Cidade();
		c2.setNome("Cidade 2");
		
		estado.addCidade(c1);
		estado.addCidade(c2);
		
		Rio rio1 = new Rio("Rio Negro", 500);
		Rio rio2 = new Rio("Solimoes", 1000);
		
		List<Rio> rios = new ArrayList<Rio>();
		rios.add(rio1);
		rios.add(rio2);
		
		c1.setRios(rios);
		
		try {
			//estadoDao.cadastrar(estado);
			capitalDao.cadastrar(capital);
			capitalDao.commit();
		} catch (CommitException e) {
			e.printStackTrace();
			fail("Falha no teste");
		}
		
		assertNotEquals(0, estado.getCodigo());
		assertNotEquals(0, capital.getCodigo());
		
		//Buscar a capital cadastrada
		try {
			Capital busca = capitalDao.buscar(capital.getCodigo());
			
			assertNotNull(busca);
			assertNotNull(busca.getEstado());
			
		} catch (ChaveInvalidaException e) {
			e.printStackTrace();
			fail("Falha no teste");
		}
		
	}

}




