package br.com.fiap.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="T_MOTORISTA")
public class Motorista {

	@Id
	@Column(name="nr_carteira")
	private int nrCarteira;
	
	@Column(name="nm_motorista", nullable = false, length = 150)
	private String nmMotorista;
	
	@Temporal(TemporalType.DATE)
	@Column(name="dt_nascimento")
	private Calendar dataNascimento;
	
	@Column(name="fl_carteira")
	private byte[] foto;
	
	@Column(name="ds_genero")
	private Sexo genero;

	public Motorista() {
		super();
	}
	
	public Motorista(int nrCarteira, String nmMotorista, Calendar dataNascimento, byte[] foto, Sexo genero) {
		super();
		this.nrCarteira = nrCarteira;
		this.nmMotorista = nmMotorista;
		this.dataNascimento = dataNascimento;
		this.foto = foto;
		this.genero = genero;
	}

	public int getNrCarteira() {
		return nrCarteira;
	}

	public void setNrCarteira(int nrCarteira) {
		this.nrCarteira = nrCarteira;
	}

	public String getNmMotorista() {
		return nmMotorista;
	}

	public void setNmMotorista(String nmMotorista) {
		this.nmMotorista = nmMotorista;
	}

	public Calendar getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Calendar dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public Sexo getGenero() {
		return genero;
	}

	public void setGenero(Sexo genero) {
		this.genero = genero;
	}
	
	
	
	
}
