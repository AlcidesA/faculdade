package br.com.fiap.dao.impl;

import javax.persistence.EntityManager;

import br.com.fiap.dao.VeiculoDAO;
import br.com.fiap.entity.Veiculo;
import br.com.fiap.exception.CommitException;
import br.com.fiap.exception.NotFoundException;

public class VeiculoDAOImpl implements VeiculoDAO{

	private EntityManager em;
	
	public VeiculoDAOImpl(EntityManager em) {
		super();
		this.em = em;
	}

	@Override
	public void cadastrar(Veiculo veiculo) {
		em.persist(veiculo);
	}

	@Override
	public void deletar(int codigo) throws NotFoundException {
		Veiculo veiculo = buscar(codigo);
		
		if(veiculo == null) 
			throw new NotFoundException();
		
		em.remove(veiculo);
	}

	@Override
	public void atualizar(Veiculo veiculo) {
		em.merge(veiculo);
	}

	@Override
	public Veiculo buscar(int codigo) {
		return em.find(Veiculo.class, codigo);
	}

	@Override
	public void commit() throws CommitException {
		try {
			em.getTransaction().begin();
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			throw new CommitException();
		}
	}

}
