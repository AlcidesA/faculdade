package br.com.fiap.dao;

import br.com.fiap.entity.Motorista;
import br.com.fiap.exception.CommitException;
import br.com.fiap.exception.NotFoundException;

public interface MotoristaDAO {

	void cadastrar(Motorista motorista);
	
	Motorista buscar(int codigo);
	
	void atualizar(Motorista motorista);
	
	void deletar(int codigo) throws NotFoundException;
	
	void commit() throws CommitException;
	
}
