package br.com.fiap.dao;

import br.com.fiap.entity.Veiculo;
import br.com.fiap.exception.CommitException;
import br.com.fiap.exception.NotFoundException;

public interface VeiculoDAO {
	
	void cadastrar(Veiculo veiculo);
	
	void deletar(int codigo) throws NotFoundException;
	
	void atualizar(Veiculo veiculo);
	
	Veiculo buscar(int codigo);
	
	void commit() throws CommitException;
	
}
