package br.com.fiap.dao.impl;

import javax.persistence.EntityManager;

import br.com.fiap.dao.MotoristaDAO;
import br.com.fiap.entity.Motorista;
import br.com.fiap.exception.CommitException;
import br.com.fiap.exception.NotFoundException;

public class MotoristaDAOImpl implements MotoristaDAO {

	private EntityManager em;
	
	public MotoristaDAOImpl(EntityManager em) {
		super();
		this.em = em;
	}

	@Override
	public void cadastrar(Motorista motorista) {
		em.persist(motorista);
		
	}

	@Override
	public Motorista buscar(int codigo) {
		return em.find(Motorista.class, codigo);
	}

	@Override
	public void atualizar(Motorista motorista) {
		em.merge(motorista);
	}

	@Override
	public void deletar(int codigo) throws NotFoundException {
		Motorista motorista = buscar(codigo);
		
		if(motorista == null)
			throw new NotFoundException();
		
		em.remove(motorista);
		
	}

	@Override
	public void commit() throws CommitException {
		try {
			em.getTransaction().begin();
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			throw new CommitException();
		}
		
	}

}
