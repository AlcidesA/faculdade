package br.com.fiap.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.fiap.dao.VeiculoDAO;
import br.com.fiap.dao.impl.VeiculoDAOImpl;
import br.com.fiap.entity.Veiculo;
import br.com.fiap.exception.CommitException;
import br.com.fiap.exception.NotFoundException;

public class TesteVeiculo {

	public static void main(String[] args) {
		EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("CLIENTE_ORACLE");
		EntityManager em = fabrica.createEntityManager();
		
		VeiculoDAO dao = new VeiculoDAOImpl(em);
		
		//Cadastro
		Veiculo veiculo = new Veiculo("ASD-321","Vermelho",2010);
		try {
			dao.cadastrar(veiculo);
			dao.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//Atualizacao
		veiculo.setCor("Rosa");
		try {
			dao.atualizar(veiculo);
			dao.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		Veiculo busca = dao.buscar(1);
		System.out.println(busca.getPlaca());
		
		try {
			dao.deletar(1);
			dao.commit();
		} catch (NotFoundException e) {
			e.printStackTrace();
		} catch (CommitException e) {
			e.printStackTrace();
		}
		
		em.close();
		fabrica.close();
	}

}
