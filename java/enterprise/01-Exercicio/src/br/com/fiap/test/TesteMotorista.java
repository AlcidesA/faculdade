package br.com.fiap.test;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.fiap.dao.MotoristaDAO;
import br.com.fiap.dao.impl.MotoristaDAOImpl;
import br.com.fiap.entity.Motorista;
import br.com.fiap.entity.Sexo;
import br.com.fiap.exception.CommitException;
import br.com.fiap.exception.NotFoundException;

public class TesteMotorista {

	public static void main(String[] args) {
	
		EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("CLIENTE_ORACLE");
		EntityManager em = fabrica.createEntityManager();
		
		MotoristaDAO dao = new MotoristaDAOImpl(em);
//		
		Motorista motorista = new Motorista(
			1, 
			"Alcides AUGUSTO", 
			new GregorianCalendar(2010, Calendar.JANUARY,15), 
			null, 
			Sexo.MASCULINO 
		);
		
		try {
			dao.cadastrar(motorista);
			dao.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		motorista.setNmMotorista("Alcides Augusto");
		try {
			dao.atualizar(motorista);
			dao.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		Motorista busca = dao.buscar(1);
		System.out.println(busca.getNmMotorista());
		
		try {
			dao.deletar(2);
			dao.commit();
		} catch (NotFoundException e) {
			e.printStackTrace();
		} catch (CommitException e) {
			e.printStackTrace();
		}
		
		em.close();
		fabrica.close();

	}

}
