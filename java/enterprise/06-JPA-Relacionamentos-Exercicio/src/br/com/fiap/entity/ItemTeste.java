package br.com.fiap.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="T_EX_ITEM_TESTE")
@SequenceGenerator(name = "itemteste", sequenceName = "T_SQ_EX_ITEM_TESTE", allocationSize = 1)
public class ItemTeste {

	@Id
	@Column(name="COD_ITEM_TESTE")
	@GeneratedValue(generator = "itemteste", strategy = GenerationType.SEQUENCE)
	private int codigo;
	
	@Column(name="DES_ITEM_TESTE", nullable = false, length = 200)
	private String descricao;

	@ManyToOne
	@JoinColumn(name = "COD_CASO_TESTE")
	private CasoTeste casoTeste;
	
	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinTable(name="T_EX_ITEM_TESTE_USUARIO",
	joinColumns=@JoinColumn(name="COD_ITEM_TESTE"),
	inverseJoinColumns=@JoinColumn(name="COD_USUARIO"))
	private List<Usuario> usuario;
	
	public ItemTeste() {
		super();
	}
	
	public ItemTeste(String descricao) {
		super();
		this.descricao = descricao;
	}
	
	public ItemTeste(int codigo, String descricao) {
		super();
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public CasoTeste getCasoTeste() {
		return casoTeste;
	}

	public void setCasoTeste(CasoTeste casoTeste) {
		this.casoTeste = casoTeste;
	}

	public List<Usuario> getUsuario() {
		return usuario;
	}

	public void setUsuario(List<Usuario> usuario) {
		this.usuario = usuario;
	}
	
}
