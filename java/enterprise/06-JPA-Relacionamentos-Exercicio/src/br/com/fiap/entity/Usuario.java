package br.com.fiap.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="T_EX_USUARIO")
@SequenceGenerator(name = "usuario", sequenceName = "T_SQ_EX_USUARIO", allocationSize = 1)
public class Usuario {

	@Id
	@Column(name="COD_USUARIO")
	@GeneratedValue(generator = "usuario", strategy = GenerationType.SEQUENCE)
	private int codigo;
		
	@Column(name="NOM_USUARIO", nullable = false, length = 100)
	private String nome;
	
	@ManyToMany(mappedBy = "usuario")
	private List<ItemTeste> itemTeste;
	
	public Usuario() {
		super();
	}

	public Usuario(String nome) {
		super();
		this.nome = nome;
	}
	
	public Usuario(int codigo, String nome) {
		super();
		this.codigo = codigo;
		this.nome = nome;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<ItemTeste> getItemTeste() {
		return itemTeste;
	}

	public void setItemTeste(List<ItemTeste> itemTeste) {
		this.itemTeste = itemTeste;
	}
	
	
}
