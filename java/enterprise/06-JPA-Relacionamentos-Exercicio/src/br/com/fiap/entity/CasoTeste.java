package br.com.fiap.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "T_EX_CASO_TESTE")
@SequenceGenerator(name = "casoteste", sequenceName = "T_SQ_EX_CASO_TESTE", allocationSize = 1)
public class CasoTeste {

	@Id
	@Column(name = "COD_CASO_TESTE")
	@GeneratedValue(generator = "casoteste", strategy = GenerationType.SEQUENCE)
	private int codigo;

	@Column(name = "NOM_CASO_TESTE", nullable = false, length = 100)
	private String nome;

	@Column(name = "DES_CASO_TESTE", nullable = false, length = 200)
	private String descricao;

	@ManyToOne
	@JoinColumn(name = "COD_SISTEMA")
	private Sistema sistema;
	
	@OneToMany(mappedBy = "casoTeste", cascade = CascadeType.PERSIST)
	private List<ItemTeste> itemTeste = new ArrayList<ItemTeste>();
	
	public void addItemTeste(ItemTeste itemTeste) {
		itemTeste.setCasoTeste(this);
		this.itemTeste.add(itemTeste);
	}
	
	public CasoTeste() {
		super();
	}

	public CasoTeste(String nome, String descricao) {
		super();
		this.nome = nome;
		this.descricao = descricao;
	}
	
	public CasoTeste(int codigo, String nome, String descricao) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.descricao = descricao;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Sistema getSistema() {
		return sistema;
	}

	public void setSistema(Sistema sistema) {
		this.sistema = sistema;
	}

}
