package br.com.fiap.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "T_EX_SISTEMA")
@SequenceGenerator(name = "sistema", sequenceName = "T_SQ_EX_SISTEMA", allocationSize = 1)
public class Sistema {

	@Id
	@Column(name = "COD_SISTEMA")
	@GeneratedValue(generator = "sistema", strategy = GenerationType.SEQUENCE)
	private int codigo;
	
	@Column(name = "NOM_SISTEMA", nullable = false, length = 100)
	private String nome;
	
	@OneToMany(mappedBy = "sistema", cascade = CascadeType.PERSIST)
	private List<CasoTeste> casoTeste = new ArrayList<>();

	public void addCasoTeste(CasoTeste caso) {
		caso.setSistema(this);
		this.casoTeste.add(caso);
	}
	
	public Sistema() {
		super();
	}
	
	public Sistema(String nome) {
		super();
		this.nome = nome;
	}
	
	public Sistema(int codigo, String nome) {
		super();
		this.codigo = codigo;
		this.nome = nome;
	}
	
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<CasoTeste> getCasoTeste() {
		return casoTeste;
	}

	public void setCasoTeste(List<CasoTeste> casoTeste) {
		this.casoTeste = casoTeste;
	}
	
}
