package br.com.fiap.junity;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import br.com.fiap.dao.SistemaDAO;
import br.com.fiap.dao.impl.SistemaDAOImpl;
import br.com.fiap.entity.CasoTeste;
import br.com.fiap.entity.ItemTeste;
import br.com.fiap.entity.Sistema;
import br.com.fiap.entity.Usuario;
import br.com.fiap.exception.CommitException;
import br.com.fiap.singleton.EntityManagerFactorySingleton;

class Teste {

	@Test
	void test() {
		SistemaDAO dao = new SistemaDAOImpl(
			EntityManagerFactorySingleton.getInstance()
				.createEntityManager());
		
		Sistema sistema = new Sistema();
		sistema.setNome("Sistema do rei do gado");
		
		CasoTeste caso = new CasoTeste();
		caso.setNome("Contagem de gado");
		caso.setDescricao("Contar o gado atrav�s do IOT");
		
		ItemTeste item1 = new ItemTeste();
		item1.setDescricao("Contar um rebanho com 100 cabe�as");
		
		ItemTeste item2 = new ItemTeste();
		item2.setDescricao("Contar um rebanho por m2");
		
		Usuario usuario = new Usuario();
		usuario.setNome("Juninho");
		
		sistema.addCasoTeste(caso);
		
		caso.addItemTeste(item1);
		caso.addItemTeste(item2);
		
		List<Usuario> usuarios = new ArrayList<>();
		usuarios.add(usuario);
		
		item1.setUsuario(usuarios);

		try {
			dao.cadastrar(sistema);
			dao.commit();
		} catch (CommitException e) {
			e.printStackTrace();
			fail();
		}
		
	}

}



