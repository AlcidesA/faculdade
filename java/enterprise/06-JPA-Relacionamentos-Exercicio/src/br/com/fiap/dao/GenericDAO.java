package br.com.fiap.dao;

import br.com.fiap.exception.CommitException;
import br.com.fiap.exception.NotFound;

public interface GenericDAO<T, K> {

	//create
	void cadastrar(T entidade);
	
	//read
	T buscar(K chave) throws NotFound;
	
	//update
	void atualizar(T entidade);
	
	//delete
	void remover(K chave) throws NotFound;
	
	//commit
	void commit() throws CommitException;
	
	
}
