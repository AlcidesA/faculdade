package br.com.fiap.singleton;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerFactorySingleton {

	
	private static EntityManagerFactory fabrica;
	
	//construtor privado para não permitir que a classe seja instanciada
	private EntityManagerFactorySingleton() {};
	
	//metodo que permite apenas uma instancia do entity manager factory
	public static EntityManagerFactory getInstance() {
		if(fabrica == null) {
			fabrica = Persistence.createEntityManagerFactory("CLIENTE_ORACLE");
		}
		
		return fabrica;
	}
	
}
