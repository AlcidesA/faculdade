package br.com.fiap.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import br.com.fiap.loja.bo.EstoqueBOStub;
import br.com.fiap.loja.bo.EstoqueBOStub.ConsultarProduto;
import br.com.fiap.loja.bo.EstoqueBOStub.ConsultarProdutoResponse;
import br.com.fiap.loja.bo.EstoqueBOStub.ProdutoTO;

public class Tela {

	protected Shell shell;
	private Text txtCodigoProduto;
	private Text txtDescricao;
	private Text txtPreco;
	private Text txtQuantidade;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Tela window = new Tela();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(450, 300);
		shell.setText("SWT Application");
		
		Label lblCdigoProduto = new Label(shell, SWT.NONE);
		lblCdigoProduto.setBounds(10, 34, 94, 15);
		lblCdigoProduto.setText("C\u00F3digo Produto");
		
		txtCodigoProduto = new Text(shell, SWT.BORDER);
		txtCodigoProduto.setBounds(117, 31, 76, 21);
		
		Label lblDescrio = new Label(shell, SWT.NONE);
		lblDescrio.setBounds(10, 88, 55, 15);
		lblDescrio.setText("Descri\u00E7\u00E3o");
		
		Label lblPreo = new Label(shell, SWT.NONE);
		lblPreo.setBounds(10, 123, 55, 15);
		lblPreo.setText("Pre\u00E7o");
		
		Label lblQuantidade = new Label(shell, SWT.NONE);
		lblQuantidade.setBounds(10, 161, 67, 15);
		lblQuantidade.setText("Quantidade");
		
		txtDescricao = new Text(shell, SWT.BORDER);
		txtDescricao.setEditable(false);
		txtDescricao.setBounds(86, 85, 76, 21);
		
		txtPreco = new Text(shell, SWT.BORDER);
		txtPreco.setEditable(false);
		txtPreco.setBounds(86, 120, 76, 21);
		
		txtQuantidade = new Text(shell, SWT.BORDER);
		txtQuantidade.setEditable(false);
		txtQuantidade.setBounds(87, 158, 76, 21);
		
		Button btnPesquisar = new Button(shell, SWT.NONE);
		btnPesquisar.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				 
				String codigo = txtCodigoProduto.getText();
				
				try {
					//chama o ws
					EstoqueBOStub stub = new EstoqueBOStub();
					
					//cada metodo do provider se torna uma classe
					//instancia o objeto e seta o codigo a ser pesquisado
					ConsultarProduto produto = new ConsultarProduto();
					produto.setCodigo(Integer.parseInt(codigo));
					
					//chamar o ws e receber o resultado
					ConsultarProdutoResponse resp = stub.consultarProduto(produto);
					
					txtDescricao.setText(resp.get_return().getDescricao());
					txtPreco.setText(String.valueOf(resp.get_return().getPreco()));
					txtQuantidade.setText(String.valueOf(resp.get_return().getQuantidade()));
					
				} catch (Exception err) {
					//e.printStackTrace();
					//mensagem de erro amigavel
					txtDescricao.setText(err.getMessage());
				}
				
			}
		});
		btnPesquisar.setBounds(215, 28, 75, 25);
		btnPesquisar.setText("Pesquisar");

	}

}
