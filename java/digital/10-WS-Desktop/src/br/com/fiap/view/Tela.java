package br.com.fiap.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.tempuri.CalcPrecoPrazoWSStub;
import org.tempuri.CalcPrecoPrazoWSStub.CalcPrazo;
import org.tempuri.CalcPrecoPrazoWSStub.CalcPrazoResponse;

public class Tela {

	protected Shell txtDataMaxima;
	private Text txtNumero1;
	private Text txtNumero2;
	private Text txtResultado;
	private Text txtOrigem;
	private Text txtDestino;
	private Text txtPrazo;
	private Text txtDtMaxima;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Tela window = new Tela();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		txtDataMaxima.open();
		txtDataMaxima.layout();
		while (!txtDataMaxima.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		txtDataMaxima = new Shell();
		txtDataMaxima.setSize(450, 300);
		txtDataMaxima.setText("SWT Application");
		
		Label lblNmero = new Label(txtDataMaxima, SWT.NONE);
		lblNmero.setBounds(10, 23, 55, 15);
		lblNmero.setText("N\u00FAmero 1");
		
		txtNumero1 = new Text(txtDataMaxima, SWT.BORDER);
		txtNumero1.setBounds(71, 20, 76, 21);
		
		Label lblNmero_1 = new Label(txtDataMaxima, SWT.NONE);
		lblNmero_1.setBounds(10, 63, 55, 15);
		lblNmero_1.setText("N\u00FAmero 2");
		
		txtNumero2 = new Text(txtDataMaxima, SWT.BORDER);
		txtNumero2.setBounds(71, 60, 76, 21);
		
		Button btnCalcular = new Button(txtDataMaxima, SWT.NONE);
		btnCalcular.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int n1 = Integer.parseInt(txtNumero1.getText());
				int n2 = Integer.parseInt(txtNumero2.getText());
				
				int soma = n1 + n2;
				
				txtResultado.setText(String.valueOf(soma));
				
			}
		});
		btnCalcular.setBounds(71, 95, 75, 25);
		btnCalcular.setText("Calcular");
		
		txtResultado = new Text(txtDataMaxima, SWT.BORDER);
		txtResultado.setEditable(false);
		txtResultado.setBounds(71, 163, 76, 21);
		
		Label lblResultado = new Label(txtDataMaxima, SWT.NONE);
		lblResultado.setBounds(10, 166, 55, 15);
		lblResultado.setText("Resultado");
		
		Label label = new Label(txtDataMaxima, SWT.SEPARATOR | SWT.VERTICAL);
		label.setBounds(181, 23, 2, 161);
		
		Label lblCepOrigem = new Label(txtDataMaxima, SWT.NONE);
		lblCepOrigem.setBounds(203, 23, 76, 15);
		lblCepOrigem.setText("CEP Origem:");
		
		txtOrigem = new Text(txtDataMaxima, SWT.BORDER);
		txtOrigem.setBounds(285, 20, 76, 21);
		
		Label lblCepDestino = new Label(txtDataMaxima, SWT.NONE);
		lblCepDestino.setBounds(203, 63, 76, 15);
		lblCepDestino.setText("CEP Destino");
		
		txtDestino = new Text(txtDataMaxima, SWT.BORDER);
		txtDestino.setBounds(285, 60, 76, 21);
		
		Button btnPesquisar = new Button(txtDataMaxima, SWT.NONE);
		btnPesquisar.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				try {
					//Objeto que possui os m�todos do web service
					CalcPrecoPrazoWSStub stub = new CalcPrecoPrazoWSStub();
					
					CalcPrazo valores = new CalcPrazo();
					valores.setNCdServico("40010");
					
					String destino = txtDestino.getText();
					valores.setSCepDestino(destino);
					
					String origem = txtOrigem.getText();
					valores.setSCepOrigem(origem);
					
					CalcPrazoResponse resp = stub.calcPrazo(valores);
					
					txtPrazo.setText(resp.getCalcPrazoResult().getServicos().getCServico()[0].getPrazoEntrega());
					txtDtMaxima.setText(resp.getCalcPrazoResult().getServicos().getCServico()[0].getDataMaxEntrega());

					
				} catch (Exception err) {
					err.printStackTrace();
				}
				
				
				
				
				
				
				
				
				
			}
		});
		btnPesquisar.setBounds(285, 95, 75, 25);
		btnPesquisar.setText("Pesquisar");
		
		Label lblPrazo = new Label(txtDataMaxima, SWT.NONE);
		lblPrazo.setBounds(203, 136, 55, 15);
		lblPrazo.setText("Prazo");
		
		txtPrazo = new Text(txtDataMaxima, SWT.BORDER);
		txtPrazo.setEditable(false);
		txtPrazo.setBounds(285, 130, 76, 21);
		
		Label lblDataMxima = new Label(txtDataMaxima, SWT.NONE);
		lblDataMxima.setBounds(203, 169, 76, 15);
		lblDataMxima.setText("Data M\u00E1xima");
		
		txtDtMaxima = new Text(txtDataMaxima, SWT.BORDER);
		txtDtMaxima.setEditable(false);
		txtDtMaxima.setBounds(285, 163, 76, 21);

	}
}
