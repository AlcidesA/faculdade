package br.com.fiap.loja.view;

import javax.swing.JOptionPane;

import br.com.fiap.loja.bo.EstoqueBOStub;
import br.com.fiap.loja.bo.EstoqueBOStub.ConsultarProduto;
import br.com.fiap.loja.bo.EstoqueBOStub.ConsultarProdutoResponse;
import br.com.fiap.loja.bo.EstoqueBOStub.ProdutoTO;

public class ConsoleView {

	public static void main(String[] args) {
		
		int codigo = Integer.parseInt(JOptionPane.showInputDialog(null, "Digite o c�digo: "));
		
		try {
			//chama o ws
			EstoqueBOStub stub = new EstoqueBOStub();
			
			//cada metodo do provider se torna uma classe
			//instancia o objeto e seta o codigo a ser pesquisado
			ConsultarProduto produto = new ConsultarProduto();
			produto.setCodigo(codigo);
			
			//chamar o ws e receber o resultado
			ConsultarProdutoResponse resp = stub.consultarProduto(produto);
			
			ProdutoTO produtoTO = resp.get_return();
			System.out.println(produtoTO.getDescricao());
			System.out.println("Pre�o: " + produtoTO.getPreco());
			System.out.println("Quantidade: " + produtoTO.getQuantidade());
		} catch (Exception e) {
			//e.printStackTrace();
			//mensagem de erro amigavel
			System.out.println(e.getMessage());
		}
		
	}

}
